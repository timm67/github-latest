import sys
import json

import requests

# Use Like python githubber.py JASchilz
# (or another user name)

def get_last_timestamp():
    try:
        username = sys.argv[1]
    except IndexError:
        print("Please provide a username as the argument")
        return

    # 1. Retrieve a list of "events" associated with the given user name
    # 2. Print out the time stamp associated with the first event in that list.

    events_url = "https://api.github.com/users/{0}/events".format(username)

    events_req = requests.request(url=events_url, method='GET')

    num_events = len(events_req.json())
    if (num_events == 0):
        print("Username [{0}] not found".format(username))
        return
    else:
        print("{0} Events received for user [{1}]".format(num_events, username))

    # Assuming 'first' event means the zero-eth (i.e. current) one instead
    # of the least recent
    first_event = events_req.json()[0]

    print("Event {0} [{1}] occured at [{2}]".format(first_event['type'], first_event['id'], first_event['created_at']))
    events_req.close()
    return


if __name__ == "__main__":
    get_last_timestamp()